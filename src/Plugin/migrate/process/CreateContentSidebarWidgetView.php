<?php

namespace Drupal\earthdata_migrate\Plugin\migrate\process;

use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Config\FileStorage;
use Drupal\Core\Site\Settings;
use Drupal\migrate\Row;
use Drupal\views\Views;
use Drupal\views\ViewEntityInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Creates a Content Sidebar Widget View.
 *
 * See: How do I programmatically create a view in a custom module? 
 * https://drupal.stackexchange.com/questions/274828/how-do-i-programmatically-create-a-view-in-a-custom-module
 *
 * @MigrateProcessPlugin(
 *   id = "create_content_sidebar_widget",
 *   handle_multiples = TRUE
 * )
 *
 * The Conduit database contains "blocks" named "LinkListGadget".
 * This plugin creates a corresponding Content Sidebar Widget View
 * for each entity (node) which has one assigned to it.
 *
 * On the Drupal side, the field_sidebar_widget_blocks Entity reference field 
 * stores the IDs of the entities. This plugin returns a comma-separated list of 
 * the newly created entities, which then get passed as the field's value.
 *
 * @code
 *   field_sidebar_widget_blocks:
 *     plugin: create_content_sidebar_widget
 *     source: content
 * @endcode
 */
class CreateContentSidebarWidgetView extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {

    if (!is_array($value)) {
      throw new MigrateException('Input should be an array.');
    }

    $content = $mnames = [];

    // Conditional for different paths depending on the environment, specifically the existence of the "earthdata" directory.
    $base_dir = is_dir('/var/www/html/earthdata') ? '/var/www/html/earthdata' : '/var/www/html';
    $change_directory = 'cd ' . $base_dir . ' && ';

    // Get the sidebar gadgets (sidebar menus).
    // With support for the "default" (id = 8) sidebar gadget - "ESO More Resources" (it will only added once).
    if (((int)$value['sidebar_gadget_1'] > 0) || ($value['sidebar_gadget_1'] === 'default')) {
      $sidebar_gadget_ids[] = ($value['sidebar_gadget_1'] === 'default') ? 8 : $value['sidebar_gadget_1'];
    }

    if (((int)$value['sidebar_gadget_2'] > 0) || ($value['sidebar_gadget_2'] === 'default')) {
      $sidebar_gadget_ids[] = ($value['sidebar_gadget_2'] === 'default') ? 8 : $value['sidebar_gadget_2'];
    }

    if (!empty($sidebar_gadget_ids)) {

      // Get List Link Gadgets from Conduit.
      $result = $this->getListLinkGadgets($sidebar_gadget_ids);

      if (!empty($result)) {

        $links = $link_list = [];

        foreach ($result as $key => $val) {
          // Remove non alphanumeric characters from the name.
          $machine_name = preg_replace( '/[\W]/', '', $val['name']);
          $converter = new CamelCaseToSnakeCaseNameConverter();
          $content[$key]['machine_name'] = $converter->normalize($machine_name);
          $content[$key]['heading'] = $val['heading'];

          foreach ($val['links'] as $link_key => $link_parts) {
            // Trim whitespace from the URL.
            $href = trim($link_parts['href']);
            // Create the relative URL or the absolute/external URL.
            if (strstr($href, 'http://') || strstr($href, 'https://')) {
              $links[$key][] = Link::fromTextAndUrl($link_parts['text'], Url::fromUri( $href ))->toString();
            } else {
              $links[$key][] = Link::fromTextAndUrl($link_parts['text'], Url::fromUserInput( $href ));
            }
          }
          $link_list[$key] = [
            '#theme' => 'item_list',
            '#list_type' => 'ul',
            '#items' => $links[$key],
            '#attributes' => [
              // 'class' => array('names-navigation'),
            ],
          ];
          // Render the unordered list.
          $output = \Drupal::service('renderer')->renderRoot($link_list[$key]);
          $output = str_replace(array("\n", "\r"), '', $output);
          $output = preg_replace('/<!--(.|\s)*?-->/', '', $output);
          // $output = preg_replace('/\sclass=[\'|"][^\'"]+[\'|"]/', '', $output);
          // Clean-up the HTML and set the rendered content.
          include_once drupal_get_path('module', 'earthdata_migrate') . '/includes/htmLawed.php';
          $content[$key]['content'] = htmLawed($output, array('tidy' => 1, 'balance' => 0, 'unique_ids' => 0));
          $content[$key]['content'] = $output;     
        }

      }

      // Only create if the views module is enabled.
      if (\Drupal::moduleHandler()->moduleExists('views')) {

        // Get the view.
        $view = Views::getView('content_sidebar_widgets');

        if (is_object($view)) {

          $new_display = $block_data = [];

          // View config is in `../config/sync/views.view.content_sidebar_widgets.yml`
          $dir = Settings::get('config_sync_directory', FALSE);

          $fileStorage = new FileStorage($dir);
          $config = $config_diff = $fileStorage->read('views.view.content_sidebar_widgets');

          foreach ($content as $ckey => $cval) {
            // If there are differences, merge the new display into the view's display.
            if (!array_key_exists($cval['machine_name'], $config['display'])) {

              // Add the child view to the content_sidebar_widgets config.
              $new_display[$cval['machine_name']] = $this->addDisplayToView($cval);

              // Gather the machine name(s) of the block(s) so it/they can be passed as entity reference(s).
              $block_data[$ckey]['id'] = 'views_block__content_sidebar_widgets_' . $cval['machine_name'];
              $block_data[$ckey]['machine_name'] = $cval['machine_name'];

              $config['display'] = array_merge($config['display'], $new_display);
            }
          }

          // If the view config has changed, write to updated config to the filesystem.
          if (!empty($new_display)) {

            // Write the updated config.
            $new_config = $fileStorage->write('views.view.content_sidebar_widgets', $config);

            // Invalidate view cache and save the view.
            if ($new_config) {

              $view->storage->invalidateCaches();
              $view->save(TRUE);

              // Import configuration - @TODO: shell_exec works, but is there a Drupal method for this?
              shell_exec($change_directory . 'drush cim -y 2> /dev/null');

              // Create the block
              $fileStorage = new FileStorage($dir);
              // Use an existing configuration as the template.
              $config = $fileStorage->read('block.block.views_block__content_sidebar_widgets_esds_sidebar_block');
              $uuid_service = \Drupal::service('uuid');

              // Loop through block data and create the new configurations.
              foreach ($block_data as $bkey => $bval) {
                $config['uuid'] = $uuid_service->generate();
                $config['id'] = $bval['id'];
                $config['plugin'] = 'views_block:content_sidebar_widgets-' . $bval['machine_name'];
                $config['settings']['id'] = 'views_block:content_sidebar_widgets-' . $bval['machine_name'];

                // Write to filesystem if the file doesn't exist.
                if (!$fileStorage->exists('block.block.views_block__content_sidebar_widgets_' . $bval['machine_name'], $config)) {
                  // Write the new configuration to the filesystem.
                  $fileStorage->write('block.block.views_block__content_sidebar_widgets_' . $bval['machine_name'], $config);
                  // Import configuration - @TODO: shell_exec works, but is there a Drupal method for this?
                  shell_exec($change_directory . 'drush cim -y 2> /dev/null');
                }
              }

            }

          }

        }

      }

      // Return the ids of the view block(s).
      foreach ($content as $ckey => $cval) {
        $mnames[] = 'views_block__content_sidebar_widgets_' . $cval['machine_name'];
      }

      // Reverse the order of the ids.
      if (!empty($mnames)) {
        $mnames = array_reverse($mnames);
      }

    }

    return $mnames;
  }

  /**
   * Get List Link Gadgets
   *
   * @param array $ids
   * @return array An array of List Link Gadgets from conduit.
   */
  private function getListLinkGadgets($ids) {

    $data = $fields = [];

    if (!empty($ids)) {

      // Switch to the Conduit database.
      \Drupal\Core\Database\Database::setActiveConnection('conduit');
      $db = \Drupal\Core\Database\Database::getConnection();

      // Query Conduit's database.
      $result = $db->select('items','i')
        ->fields('i', ['id', 'fields'])
        ->condition('id', $ids, 'IN')
        ->execute()
        ->fetchAll();

      // Switch back to the default database.
      \Drupal\Core\Database\Database::setActiveConnection();

      if (!empty($result)) {
        foreach ($result as $key => $value) {
          $data[] = json_decode($value->fields, TRUE);
        }
      }

    }

    return $data;
  }

  /**
   * View Display Template
   *
   * @param array $content
   * @return string
   */
  private function addDisplayToView($content) {

    return [
      "display_plugin" => "block",
      "id" => $content['machine_name'],
      "display_title" => $content['heading'] . " Sidebar Block",
      "position" => 1,
      "display_options" => [
        "display_extenders" => [],
        "block_description" => $content['heading'] . " Sidebar Block",
        "display_description" => "",
        "title" => $content['heading'],
        "defaults" => [
          "title" => false,
          "fields" => false,
        ],
        "fields" => [
          "id" => [
            "id" => "id",
            "table" => "paragraphs_item_field_data",
            "field" => "id",
            "relationship" => "none",
            "group_type" => "group",
            "admin_label" => "",
            "label" => "",
            "exclude" => false,
            "alter" => [
              "alter_text" => true,
              "text" => $content['content'],
              "make_link" => false,
              "path" => "",
              "absolute" => false,
              "external" => false,
              "replace_spaces" => false,
              "path_case" => "none",
              "trim_whitespace" => false,
              "alt" => "",
              "rel" => "",
              "link_class" => "",
              "prefix" => "",
              "suffix" => "",
              "target" => "",
              "nl2br" => false,
              "max_length" => 0,
              "word_boundary" => true,
              "ellipsis" => true,
              "more_link" => false,
              "more_link_text" => "",
              "more_link_path" => "",
              "strip_tags" => false,
              "trim" => false,
              "preserve_tags" => "",
              "html" => false,
            ],
            "element_type" => "",
            "element_class" => "",
            "element_label_type" => "",
            "element_label_class" => "",
            "element_label_colon" => false,
            "element_wrapper_type" => "",
            "element_wrapper_class" => "",
            "element_default_classes" => true,
            "empty" => "",
            "hide_empty" => false,
            "empty_zero" => false,
            "hide_alter_empty" => true,
            "click_sort_column" => "value",
            "type" => "number_integer",
            "settings" => [
              "thousand_separator" => "",
              "prefix_suffix" => true,
            ],
            "group_column" => "value",
            "group_columns" => [],
            "group_rows" => true,
            "delta_limit" => 0,
            "delta_offset" => 0,
            "delta_reversed" => false,
            "delta_first_last" => false,
            "multi_type" => "separator",
            "separator" => ", ",
            "field_api_classes" => false,
            "entity_type" => "paragraph",
            "entity_field" => "id",
            "plugin_id" => "field",
          ],
        ],
      ],
      "cache_metadata" => [
        "max-age" => -1,
        "contexts" => [
          0 => "languages:language_content",
          1 => "languages:language_interface",
        ],
        "tags" => []
      ],
    ];

  }

}
