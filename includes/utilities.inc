<?php

use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\Component\Utility\Unicode;
use Drupal\Core\StreamWrapper\PublicStream;
use Drupal\redirect\Entity\Redirect;
use Drupal\Component\Utility\UrlHelper; 

/**
 * JSON Decode
 * 
 * JSON decode the content.
 * @param string $json
 * @return array
 */
function ed_json_decode($json) {

  $data = [];

  if (!empty($json)) {
    $data = json_decode($json, TRUE);
  }

  return $data;
}

/**
 * Import and Replace CDN Assets
 * 
 * Gather links of CDN-based assets within the body's HTML, 
 * import into Drupal, update links to point to Drupal's filesystem.
 * @param string $body
 * @return string
 */
function ed_import_replace_cdn_assets($body) {

  if (!empty($body)) {

    // IMAGES (jpg, png, gif)
    
    $images_source = 'cdn.earthdata.nasa.gov/conduit/upload/';
    $destination = 'public://imported/';

    // preg_match_all('/<img[^>]+>/i', $body, $result);

    $dom = new DOMDocument();
    $dom->preserveWhiteSpace = false;
    // See: https://stackoverflow.com/questions/8218230/php-domdocument-loadhtml-not-encoding-utf-8-correctly
    $dom->loadHTML(mb_convert_encoding($body, 'HTML-ENTITIES', 'UTF-8'), LIBXML_NOWARNING | LIBXML_NOERROR | LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

    // Get all anchors
    // $list = $dom->getElementsByTagName('img');
    $selector = new DOMXPath($dom);
    $list = $selector->query('//img');

    foreach($list as $lkey => $item) {

      if ( strstr($item->getAttribute('src'), $images_source) ) {

        $filepath = $item->getAttribute('src');

        if (!empty($filepath)) {

          // Create file object from a locally copied file.
          $filename = basename($filepath);

          if (filter_var($filepath, FILTER_VALIDATE_URL)) { 
            $file_contents = file_get_contents($filepath);
          } else {
            $file_contents = file_get_contents($images_source . $filepath);
          }

          $new_destination = $destination . $filename;

          if (!empty($file_contents)) {

            if ($file = file_save_data($file_contents, $new_destination, \Drupal\Core\File\FileSystemInterface::EXISTS_REPLACE)) {

              $alt = !empty($item->getAttribute('alt')) ? Unicode::truncate(str_replace('"', '', $item->getAttribute('alt')), 512) : '';
              $title = !empty($item->getAttribute('title')) ? Unicode::truncate(str_replace('"', '', $item->getAttribute('title')), 1024) : $alt;
              
              // Create media entity using saved file.
              $media = Media::create([
                'bundle' => 'image',
                'uid' => \Drupal::currentUser()->id(),
                'langcode' => \Drupal::languageManager()->getDefaultLanguage()->getId(),
                'thumbnail' => [
                  'target_id' => $file->id(),
                  'alt' => $alt,
                  'title' => $title,
                ],
                'field_media_image' => [
                  'target_id' => $file->id(),
                  'alt' => $alt,
                  'title' => $title,
                ],
              ]);

              $media->setName( $file->getFilename() )->setPublished(TRUE)->save();

              $uuid = ed_get_media_uuid($file->id());
              // Get the image caption.
              $image_caption = ed_get_image_caption($item->getAttribute('data-id'));
              // Get the image alignment.
              $image_alignment = ed_get_image_alignment($file->getFilename(), $body);
              // Get the image size.
              $image_size = ed_get_image_size($file->getFilename(), $body);

              // @TODO: Polish-up image styles. Essentially, we need styles based on max-width.
              //
              // Currently we have:
              // small_250x250_
              // medium_480x480_
              // large_700x700_
              //
              // data-size="' . $image_size['data-size'] . '"
              // data-view-mode="' . $image_size['data-view-mode'] . '"

              // Render the image tag for the replacement.
              $img_tag = $dom->saveXML($item);
              $img_tag = str_replace('/>', ' />', $img_tag);

              // If the image caption is present, format for Drupal's <figure> format.
              if (!empty($image_caption)) {
                // Create a <drupal-media> element.
                $drupal_media = $dom->createElement('drupal-media');
                $drupal_media->setAttribute('data-view-mode', $image_size['data-view-mode']);
                $drupal_media->setAttribute('data-size', $image_size['data-size']);
                $drupal_media->setAttribute('data-align', $image_alignment['captioned']);
                $drupal_media->setAttribute('data-caption', $image_caption);
                $drupal_media->setAttribute('data-entity-type', 'media');
                $drupal_media->setAttribute('data-entity-uuid', $uuid);
                $drupal_media_tag = $dom->saveHTML($drupal_media);

                // Replace the <img> element with the <drupal-media> element.
                $body = str_replace($img_tag, $drupal_media_tag, $body);
                // Remove any empty <p> tags.
                $body = str_replace('<p></p>', '', $body);
              }

              // If the image caption is not present, format as a simple <img> tag.
              if (empty($image_caption)) {

                $file_local_url = str_replace('public://', '', $file->getFileUri());
                $public_path = '/' . PublicStream::basePath() . '/';
                $item->setAttribute('src', $public_path . $file_local_url);
                $item->setAttribute('data-size', $image_size['data-size']);
                $item->setAttribute('width', $image_size['data-width']);

                // Create a <div> for the image so it can be properly aligned.
                $div = $dom->createElement('div');
                $div->setAttribute('class', $image_alignment['uncaptioned']);
                // Append the <img> element.
                $div->appendChild($item);
                $div_tag = $dom->saveHTML($div);

                // Replace the <img> element with the <div><img> elements.
                $body = str_replace($img_tag, $div_tag, $body);
                // Remove any empty <p> tags.
                $body = str_replace('<p></p>', '', $body);
              }

            }

          }

        }
      }
    }

    // DOCUMENTS (pdf, doc, xls, etc.)

    $prepared_urls = [];
    $filesystem = \Drupal::service('file_system');

    // Gather all CDN-related files from the body's HTML.
    // $pattern = '#\bhttps?://cdn.earthdata.nasa.gov/conduit/upload/+[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#';
    // $pattern = '/^https?:\/\/cdn.earthdata.nasa.gov/conduit/upload/+[A-Za-z0-9\.-]+"/i';

    // Regular expressions are tricky. Let's use DOMDocument.
    $dom = new DOMDocument();
    $dom->preserveWhiteSpace = false;
    // See: https://stackoverflow.com/questions/8218230/php-domdocument-loadhtml-not-encoding-utf-8-correctly
    $dom->loadHTML(mb_convert_encoding($body, 'HTML-ENTITIES', 'UTF-8'), LIBXML_NOWARNING | LIBXML_NOERROR | LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
    // Get all anchors
    $list = $dom->getElementsByTagName('a');
    // Get all cdn.earthdata.nasa.gov urls.
    $prepared_urls = [];
    foreach($list as $item) {
      $item = parse_url($item->getAttribute('href'));
      if ($item['host'] === 'cdn.earthdata.nasa.gov') {
        $prepared_urls[] = $item['scheme'] . '://' .  $item['host'] . $item['path'];
      }
    }

    if (!empty($prepared_urls)) {

      foreach ($prepared_urls as $key => $cdn_url) {

        // Download the file and save it into Drupal's filesystem.
        $file_data = file_get_contents($cdn_url, TRUE);

        // Error handling.
        if ($file_data === FALSE) {
          throw new MigrateSkipRowException('Skipping media migration: ' . $cdn_url);
        }

        // Only process if the $file_data is retrieveable.
        if (($file_data !== FALSE) && !empty($cdn_url)) {

          // Set the file name.
          $file_name = $filesystem->basename($cdn_url);
          // Save the file.
          $file_save = file_save_data($file_data, 'public://imported/' . $file_name, \Drupal\Core\File\FileSystemInterface::EXISTS_REPLACE);
          // Set the file mimetype.
          $file_mimetype = mime_content_type('public://imported/' . $file_name);

          // Create the image media entity with the saved file entity.
          if (!strstr($file_mimetype, 'image')) {

            // Create the file entity.
            $file = File::create();
            $file->setFileUri('public://imported/' . $file_name);
            $file->setOwnerId(\Drupal::currentUser()->id());
            $file->setMimeType($file_mimetype);
            $file->setFileName($file_name);
            $file->setPermanent();
            $file->save();

            if ($file) {

              // Create the file media entity with the saved file entity.
              $media = Media::create([
                'bundle' => 'document',
                'uid' => \Drupal::currentUser()->id(),
                'field_media_document' => [
                  'target_id' => $file->id(),
                ],
               ]);

              $media->setName( $file->getFilename() )->setPublished(TRUE)->save();

              // Replace the CDN-related media asset with the media asset in Drupal's filesystem.
              $body = str_ireplace($cdn_url, '/sites/default/files/imported/' . $file_name, $body);
            }

          }

        }

      }

    }

  }

  return $body;
}

/**
 * Get Media UUID by File ID.
 */
function ed_get_media_uuid($file_id) {

  $media_entities = \Drupal::entityTypeManager()->getStorage('media')->loadByProperties([
    'field_media_image' => (int)$file_id,
  ]);

  $single_entity = array_pop($media_entities);
  $uuid = $single_entity->get('uuid')->value;

  return $uuid;
}

/**
 * Get Image Caption
 * Get the image caption from Conduit's database.
 */
function ed_get_image_caption($id) {

  $data = '';

  // Switch to the Conduit database.
  \Drupal\Core\Database\Database::setActiveConnection('conduit');
  $db = \Drupal\Core\Database\Database::getConnection();

  // Query Conduit's database.
  $result = $db->select('images','i')
    ->fields('i', ['caption'])
    ->condition('id', $id, 'LIKE')
    ->execute()
    ->fetch();

  // Switch back to the default database.
  \Drupal\Core\Database\Database::setActiveConnection();

  if (!empty($result)) {
    // Remove line breaks from the string.
    $data = preg_replace("/\r|\n/", ' ', $result->caption);
    // $data = strip_tags($data);
    $data = trim($data);
    // $data = htmlentities($data);
  }

  return $data;
}

/**
 * Get Image Alignment
 */
function ed_get_image_alignment($filename, $body) {

  $data = [];

  $dom = new DOMDocument();
  $dom->preserveWhiteSpace = false;
  $dom->loadHTML($body, LIBXML_NOWARNING | LIBXML_NOERROR);
  $xpath = new DomXPath($dom);
  // Find all image tags.
  $images = $xpath->query("//img");
  // Extract the image alignment.
  foreach($images as $key => $i) {
    if (strstr($i->getAttribute('src'), $filename)) {
      
      if (!empty($i->getAttribute('data-position'))) {
        switch ($i->getAttribute('data-position')) {
          case 'left':
            $data['captioned'] = 'left';
            $data['uncaptioned'] = 'text-left float-start';
            break;
          case 'right':
            $data['captioned'] = 'right';
            $data['uncaptioned'] = 'text-right float-end';
            break;
          case 'block':
            $data['captioned'] = 'center';
            $data['uncaptioned'] = 'text-center float-none';
            break;
          default:
            $data['captioned'] = 'center';
            $data['uncaptioned'] = 'text-center';
            break;
        }
      }

    }
  }

  return $data;
}

/**
 * Get Image Size
 */
function ed_get_image_size($filename, $body) {

  $data = [];

  $dom = new DOMDocument();
  $dom->preserveWhiteSpace = false;
  $dom->loadHTML($body, LIBXML_NOWARNING | LIBXML_NOERROR);
  $xpath = new DomXPath($dom);
  // Find all image tags.
  $images = $xpath->query("//img");
  // Extract the image alignment.
  foreach($images as $key => $i) {
    if (strstr($i->getAttribute('src'), $filename)) {
      
      if (!empty($i->getAttribute('data-size'))) {
        switch ($i->getAttribute('data-size')) {
          case 'small':
            $data['data-size'] = 'small';
            $data['data-width'] = '250';
            $data['data-view-mode'] = 'small_250x250_';
            break;
          case 'medium':
            $data['data-size'] = 'medium';
            $data['data-width'] = '400';
            $data['data-view-mode'] = 'medium_480x480_';
            break;
          case 'large':
            $data['data-size'] = 'large';
            $data['data-width'] = '600';
            $data['data-view-mode'] = 'large_700x700_';
            break;
          case 'original':
            $data['data-size'] = 'full';
            $data['data-width'] = '';
            $data['data-view-mode'] = 'full';
            break;
          default:
            $data['data-size'] = 'full';
            $data['data-width'] = '';
            $data['data-view-mode'] = 'full';
        }
      }

    }
  }

  return $data;
}

/**
 * Map Author
 * 
 * Associate the Conduit author with an existing Drupal user.
 * @param string $author_name
 * @return array
 */
function ed_map_author($author_name) {  
  // Default if the Conduit's author doesn't map to a Drupal user.
  $data['uid'] = '92'; // Joshua's user id in Drupal.
  $user_map = ed_user_map();
  $data['author_name'] = !empty($author_name) ? $author_name : 'Joshua C. Blumenfeld, NASA ESDS Managing Editor';

  if (in_array($data['author_name'], $user_map)) {
    $username = array_search($author_name, $user_map);
    $user = user_load_by_name($username);
    $data['uid'] = $user->id();
  }

  return $data;
}

/**
 * Get URL Alias
 * 
 * Get a record's url alias from Conduit.
 * @param string $id
 * @return string example: /foo/bar
 */
function ed_get_url_alias($id) {

  $data = '';

  // Switch to the Conduit database.
  \Drupal\Core\Database\Database::setActiveConnection('conduit');
  $db = \Drupal\Core\Database\Database::getConnection();

  // SELECT 'ancestry'
  // FROM 'pages'
  // WHERE 'id' = 1899
  // AND 'ancestry' IS NOT NULL

  // Query Conduit's database.
  $result = $db->select('pages','p')
    ->fields('p', ['ancestry', 'slug'])
    ->condition('id', (int)$id, '=')
    ->isNotNull('ancestry')
    ->distinct()
    ->execute()
    ->fetch();

  if ($result) {

    // Create an array from: 1843/1850/1899
    $ancestry_array = explode('/', $result->ancestry);

    // Convert id strings into integers.
    $path_ids = [];
    foreach ($ancestry_array as $key => $value) {
      $path_ids[] = (int)$value;
    }

    // SELECT id, slug
    // FROM 'pages'
    // WHERE 'id' IN (1843,1850,1899)

    // Query Conduit's database.
    foreach ($path_ids as $pkey => $pvalue) {
      $records[$pkey] = $db->select('pages','p')
        ->fields('p', ['slug'])
        ->condition('id', $pvalue, '=')
        ->execute()
        ->fetch();
    }

    $path_array = [];
    foreach ($records as $rkey => $rvalue) {
      $path_array[] = $rvalue->slug;
    }

    // Add the current page's slug to the end of the path.
    $path_array[] = $result->slug;

    // Reverse the array order.
    // $path_array = array_reverse($path_array);
    $data = '/' . implode('/', $path_array);
  }

  // Switch back to the default database.
  \Drupal\Core\Database\Database::setActiveConnection();

  return $data;
}

/**
 * Get All Paths
 * 
 * Get all distinct paths from Conduit.
 * @return array
 */
function ed_get_all_paths() {

  $data = [];

  // Switch to the Conduit database.
  \Drupal\Core\Database\Database::setActiveConnection('conduit');
  $db = \Drupal\Core\Database\Database::getConnection();

  // SELECT DISTINCT "ancestry"
  // FROM "pages"
  // WHERE "ancestry" IS NOT NULL

  // Query Conduit's database.
  $result = $db->select('pages','p')
    ->fields('p', ['ancestry'])
    // 196: A Voyage of Discovery: Applying International Metadata Standards to NASA Earth Science Data
    // ->condition('id', '196', '=')
    ->isNotNull('ancestry')
    ->distinct()
    ->execute()
    ->fetchAll();

  if (!empty($result)) {

    foreach ($result as $rk => $rv) {

      $ancestry_array = explode('/', $rv->ancestry);

      // Convert id strings into integers.
      $path_ids = [];
      foreach ($ancestry_array as $key => $value) {
        $path_ids[] = (int)$value;
      }

      // SELECT id, slug
      // FROM "pages"
      // WHERE "id" IN (1843,1850,1899)

      // Query Conduit's database.
      $records = $db->select('pages','p')
        ->fields('p', ['slug'])
        ->condition('id', $path_ids, 'IN')
        ->execute()
        ->fetchAll();

      $path_array = [];
      foreach ($records as $rkey => $rvalue) {
        $path_array[] = $rvalue->slug;
      }

      $path_array = array_reverse($path_array);
      $rv_ancestry = (string)$rv->ancestry;
      $data[$rv_ancestry] = '/' . implode('/', $path_array);

    }

  }

  // Switch back to the default database.
  \Drupal\Core\Database\Database::setActiveConnection();

  return $data;
}

/**
 * Conduit Content Types
 *
 * @return array
 */
function ed_conduit_content_types() {

  return [
    'Article',
    'NewsItem',
    'Event',
    'BasicPage',
    'HomepageMasthead',
  ];

}

/**
 * User Map
 *
 * @return array
 */
function ed_user_map() {

  return [
    'avoiland' => 'Adam Voiland',
    'chall' => 'Cynthia Hall',
    'ddavies' => 'Diane K. Davies',
    'ecassidy' => 'Emily Cassidy',
    'estolte' => 'Beth Stolte',
    'jblumenfeld' => 'Joshua C. Blumenfeld',
    'kkelly' => 'Katherine Kelly',
    'kward' => 'Kevin Ward',
    'mcechini' => 'Matthew Cechini',
    'mreese' => 'Mark Reese',
    'mwong' => 'Minnie Wong',
    'pland' => 'Paula Land',
    'rbagwell' => 'Ross E. Bagwell',
    'sberrick' => 'Stephen Berrick',
    'tgelabert' => 'Teodoro E. Gelabert',
  ];

}

/**
 * Slug Blacklist
 *
 * @return array
 */
function ed_slug_blacklist() {

  return [
    // Manully created content during the Alpha phase.
    'measuring-mangroves',
    '2019-access-projects-funded',
    'pangeo-project',
    'getting-started-with-sar',
    'counting-trees-africas-drylands',
    'a-harmonious-new-dataset',
    'solving-data-puzzle-of-sea-level-rise',
    'viirs-nrt-dtaod-product',
    'pod-sea-level-rise',
    'csda-small-scale-agriculture',
    'what-s-your-vector',
    'birds-extreme-weather',
    'user-profile-dr-steven-d-miller',
    'data-chat-dr-carmen-boening',
    'csdap',
    'impact',
    'access',
    'snow-in-the-american-rockies',
    'user-profile-dr-navin-ramankutty',
    'esds-program-components',
    'cloud',
    'measures',
    'daacs',
    'asdc',
    'asf',
    'cddis',
    'gesdisc',
    'ghrc',
    'laads',
    'lpdaac',
    'nsidc',
    'obdaac',
    'ornl',
    'podaac',
    'sedac',
    'esa-metrics',
    'advanced',
    'order-form',
    'post-form',
    'survey-form',
    'earth-observation-data',
    'idn',
    'near-real-time',
    'collaborate',
    'learn',
    'articles',
    'discipline',
    'atmosphere',
    'cryosphere',
    'human-dimensions',
    'land',
    'ocean',
    'radiance',
    'worldview-image-archive',
  ];
  
}

/**
 * Backgrounder Titles
 *
 * @return array
 */
function ed_backgrounder_titles() {

  return [
    'What is Data Latency?',
    'Essential Variables',
    'What is Remote Sensing?',
    'Sustainable Development Goals',
    'What is Synthetic Aperture Radar?',
    'Nighttime Lights',
    'Remote Sensors',
    'Active Sensors',
    'Passive Sensors',
  ];
  
}

/**
 * Update the length of a text field which already contains data.
 *
 * @param string $entity_type_id
 * @param string $field_name
 * @param integer $new_length
 */
function ed_change_text_field_max_length($entity_type_id, $field_name, $new_length) {
  $name = 'field.storage.' . $entity_type_id . "." . $field_name;

  // Get the current settings
  $result = \Drupal::database()->query(
    'SELECT data FROM {config} WHERE name = :name',
    [':name' => $name]
  )->fetchField();
  $data = unserialize($result);
  $data['settings']['max_length'] = $new_length;

  // Write settings back to the database.
  \Drupal::database()->update('config')
    ->fields(['data' => serialize($data)])
    ->condition('name', $name)
    ->execute();

  // Update the value column in both the _data and _revision tables for the field
  $table = $entity_type_id . "__" . $field_name;
  $table_revision = $entity_type_id . "_revision__" . $field_name;
  $new_field = ['type' => 'varchar', 'length' => $new_length];
  $col_name = $field_name . '_value';
  \Drupal::database()->schema()->changeField($table, $col_name, $col_name, $new_field);
  \Drupal::database()->schema()->changeField($table_revision, $col_name, $col_name, $new_field);

  // Flush the caches.
  drupal_flush_all_caches();
}

/**
 * Migrate URL Redirects
 * 
 * Migrate URL redirects from Conduit.
 * @return array
 */
function ed_migrate_url_redirects() {

  $data = $csv_array = [];

  $file = \Drupal::service('file_system')->realpath('public://conduit-redirects-teddy.csv');
  $csv_array = ed_read_csv($file);

  // Switch to the Conduit database.
  \Drupal\Core\Database\Database::setActiveConnection('conduit');
  $db = \Drupal\Core\Database\Database::getConnection();

  // SELECT pattern, target
  // FROM "redirects"

  // Query Conduit's database.
  $data = $db->select('redirects','r')
    ->fields('r', ['pattern', 'target'])
    ->execute()
    ->fetchAll(PDO::FETCH_ASSOC);

  if (!empty($data)) {

    sort($data);
    sort($csv_array);

    // Merge the CSV and database results.
    $data = array_replace($data, $csv_array);

    foreach ($data as $key => $value) {
      // Import the URL redirect into Drupal.
      $res = ed_generate_url_redirect($value);
      // Pause for 1/4 of a second, just to be nice to the database.
      usleep(250);
    }
  }

  // Switch back to the default database.
  \Drupal\Core\Database\Database::setActiveConnection();

  return $data;
}

/**
 * Read CSV
 * 
 * @param string $file
 * @return array
 */
function ed_read_csv($file) {

  $data = $csv_data = [];

  $csv = file_get_contents($file);

  if (!empty($csv)) {
    $csv_data = array_map('str_getcsv', explode("\n", $csv));
    unset($csv_data[0]);
    $csv_data = array_values($csv_data);

    if (!empty($csv_data)) {
      foreach ($csv_data as $key => $value) {
        $data[$key]['pattern'] = $value[0];
        $data[$key]['target'] = $value[1];
      }
    }
  }

  return $data;
}

/**
 * Migrate URL redirect from Conduit.
 * 
 * See: https://www.drupal.org/project/redirect/issues/2979201#comment-13770513
 * @param array $redirect
 * @return string
 */
function ed_generate_url_redirect($redirect = []) {

  $message = 'Redirect not created and may already exist. Also, please make sure the array includes the pattern and target.';

  if (is_array($redirect) && !empty($redirect) && !strstr($redirect['pattern'], '/urs?')) {

    $url_parts = UrlHelper::parse($redirect['pattern']);

    $query = \Drupal::entityTypeManager()->getStorage('redirect')->getQuery();
    $query->condition('redirect_source.path', ltrim($url_parts['path'], '/'));

    if (!empty($url_parts['query'])) {
      $query->condition('redirect_source.query', $url_parts['query']);
    }

    if (!$redirect_ids = $query->execute()) {

      unset($url_parts['fragment']);
      unset($url_parts['query']);

      $url_scheme = (!strstr($redirect['target'], 'http') && !strstr($redirect['target'], 'https')) ? 'internal:/' : '';

      Redirect::create([
        'redirect_source' => ltrim($url_parts['path'], '/'),
        'redirect_redirect' => $url_scheme . $redirect['target'],
        'status_code' => '301',
      ])->save();

      $message = t('Redirect to @target from @pattern', ['@target' => $redirect['target'], '@pattern' => $redirect['pattern']]);

      \Drupal::logger('Earthdata Migrate')->info($message);
    }

  }

  return $message;
}

/**
 * Tests and Manual Operations.
 */
function ed_tests_and_manual_operations() {

  // dd('test');

  // $paths = ed_get_all_paths();
  // dd($paths);

  // $url_alias = ed_get_url_alias('431'); // 431, 1523, 58, 760, 89, 2370 (Mangroves)
  // dd($url_alias);

  // Schema Fix

  // ed_change_text_field_max_length('node', 'field_description', 300);
  // dd('Done with field modification');

  // Migrate URL redirects from Conduit.
  // $example_redirect = [
  //   'source_url' => 'source/url/blob',
  //   'target_url' => 'internal:/learn/sensing-our-planet/blob'
  // ];
  // $result = ed_migrate_url_redirects();
  // dump($result);

  dd('done!');
}