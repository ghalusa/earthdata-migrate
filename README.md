# Earthdata Migrate Module

## Contents of this File

* Introduction
* Requirements
* Installation
* Usage (work in progress)
* Maintainers

### Introduction
 
The Earthdata Migrate module provides the functionality to migrate data from Conduit to Drupal 9.

### Requirements

* Migrate
* Migrate Tools
* Migrate Plus

### Installation

* Install as you would normally install a contributed Drupal module.
   See: [Installing Modules](https://www.drupal.org/docs/extending-drupal/installing-modules) for further information.

### Usage

#### Migrate Page

```
admin/structure/migrate
```

#### Migrate Group Page

```
admin/structure/migrate/manage/earthdata_migrate/migrations
```

#### Re-read the `config/install` directory with:

```
drush config-import --partial --source=modules/custom/earthdata_migrate/config/install
```

This needs to be executed whenever updating YML configurations.

##### General Workflow

1. Import (via `drush migrate:import` *)
1. Rollback (via `drush migrate:rollback` *)
1. Make desired adjustments to YML configuration files
1. `drush config-import --partial --source=modules/custom/earthdata_migrate/config/install`
1. Rinse, repeat

### Maintainers/Support

* [SSAI](https://www.ssaihq.com/)
* Goran Halusa: [goran.halusa@nasa.gov](goran.halusa@nasa.gov)